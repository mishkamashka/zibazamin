'use strict';
$(document).ready(function($) {


	// вспомогательная функция переключения класса .active
	// ======================================================
	$.fn.switchClass = function(elClass) {
		if (elClass === undefined) {
			var elClass = 'active';
		}
		var $self = $(this);
		$self.addClass(elClass)
			 .siblings().removeClass(elClass);
		return $self;
	}


	// Запрет на "hover" при скроллинге страницы
	// ==========================================
	var body = document.body,
		timer;

	window.addEventListener('scroll', function() {
		clearTimeout(timer);
		if(!body.classList.contains('disable-hover')) {
			body.classList.add('disable-hover')
		}
		timer = setTimeout(function(){
			body.classList.remove('disable-hover')
		},300);
	}, false);



	// Яндекс.Карты
	// ============
	var $map = $('#map');
	if ( $map.length > 0 ) {
		var arCoord = [
			[47.24056,39.71056],
			[43.410000, 39.932778],
			[60.711944, 28.729444],
			[60.618333, 28.560833],
			[54.668889, 20.403889],
			[59.880556, 30.199167],
			[46.316667, 47.994444],
			[46.623889, 142.767222],
			[59.534167, 150.766944],
			[52.237222, 141.511667],
			[49.087778, 140.271667],
			[43.113333, 131.885556],
			[68.973611, 33.059167],
			[64.534444, 40.513333],
			[71.633056, 128.889444],
			[64.736389, 177.514167],
			[63.063056, 179.355556],
			[44.617000, 33.533000],
			[27.20000, 56.25000],
			[41.01000, 28.96028],
			[31.25000, 32.28333],
			[27.41667, 17.78333],
			[34.10000, 9.65000],
			[35.30000, 38.63333],
			[21.54278, 39.17278],
			[25.26972, 55.30944],
			[49.49000, 0.10000],
			[38.43333, 15.90000],
			[40.63333, 22.95000],
			[39.46667, -0.37500],
			[41.38333, 2.18333],
			[38.71667, -9.16667],
			[51.21667, 4.40000],
			[-34.083524, 151.152404],
			[8.951948, -79.579004]
		];

		ymaps.ready(function () {
			var myMap = new ymaps.Map('map', {
				center: [36.623236, 63.415933],
				zoom: 2,
				controls: ["zoomControl", "fullscreenControl"]
			});
			myMap.behaviors.disable('scrollZoom');


			for (var i = 0; i < arCoord.length; i++) {
				var myPlacemark = new ymaps.Placemark(arCoord[i], {
					hintContent: '',
					balloonContent: '',
				}, {
					iconLayout: 'default#image',
					iconImageHref: '/build/img/map.png',
					iconImageSize: [23, 46],
					iconImageOffset: [-12, -42],
				});

				myMap.geoObjects.add(myPlacemark);

			}

		});
	}


	// Функция рассчета максимального и минимального числа в массиве
	// =================================================================
	Array.max = function( array ){return Math.max.apply( Math, array ); };
	Array.min = function( array ){return Math.min.apply( Math, array ); };
	$.fn.alignTheHeight = function() {
		var $elements = $(this);
		if ($elements.length === 0)
			return;

		// откладываем загрузку чтобы рассчет был верный
		$(window).on('load', { elements: $elements }, function(event) {

			var $elements = event.data.elements;
			var arHeights = [];

			if ($elements.length <= 0) {
				return;
			}

			$elements.each(function(index, el) {
				arHeights.push($(el).height());
			});

			var maxHeight = Array.max(arHeights);
			$elements.height(maxHeight);

			return $elements;
		});
	}


	// Выравнивание блока
	$('.service-slider__bot').alignTheHeight();


	// Слайдер "Для кого сервис"
	// =================================
	var $slider = $('.service-slider');
	if ($slider.length > 0) {
		var slickStart = function(){
			$slider.slick({
				slidesToShow: 1,
				//autoplay: true,
				slicklidesToScroll: 1,
				infinite: true,
			});
		};
		slickStart();
	}


	// Табы
	// ======================
	if ( $('.custom-tabs').length > 0 ) {
		$('.custom-tabs__titles-item').on('click', function(event) {
			var $self = $(this);
			if (!$self.hasClass('active')) {
				var index = $self.index();
				$('.process-slider__item--'+ (index+1)).switchClass('active');
				$('.custom-tabs__contents-item').eq(index).switchClass();
				$self.switchClass();
			}
		});
	}

	// задаем высоту блока
	var $window = $(window);
	var minheight = 650;
	var windowHeight = function(){
	var height = $window.height();
		if (height < minheight) {
			return minheight;
		}
		return $window.height();
	}
	var resizeFullScreenElements = function(){
		$('.init-slide, .init-wrap').css('height', (windowHeight()+40)+'px');
	};
	$window.on('resize', resizeFullScreenElements);
	resizeFullScreenElements();

	var lastVolume = 0;
	for (var i = 1; i <= 3; i++) {
		$('.init-slide--'+i)
			.attr('data-'+lastVolume, 'top:0px;')
			.attr('data-'+(lastVolume + windowHeight()), 'top:-'+windowHeight()+'px;');
		lastVolume += windowHeight();
	}
	$('.init').height(lastVolume);

	var s = skrollr.init();
	if (s.isMobile()) {
		s.destroy();
		$('.init-slide').addClass('not-fixed');
		$('.init').addClass('height-auto');
	}




	// Логика шапки и меню
	// ===================
	var $header = $('.header');
	document.headerResize = windowHeight() - 40;

	$('.top-menu__link, .logo, .logo-fix').on('click', function(event) {
		event.preventDefault();
		$.scrollTo( $(this).attr('href'), 1000);
	});

	// параметры секций
	var SectionAdvantagesOffset = $('.advantages-section').offset().top - 20;
	var SectionProcessOffset    = $('.process-section').offset().top - 20;
	var SectionContactsOffset   = $('.contacts-section').offset().top - 20;
	var SectionContactsHeight   = $('.contacts-section').height();
	var bodyHeight              = $('body').height();
	var ContactsVisibleHeight   = bodyHeight - windowHeight();

	$(window).on('scroll', function(){
		var scrollSize = $('body').scrollTop() || $('html').scrollTop();

		// переход меню
		var hasClass   = $header.hasClass('fix');
		if ((scrollSize >= document.headerResize) && !hasClass) {
			$header.addClass('fix');
		} else if((scrollSize < document.headerResize) && hasClass) {
			$header.removeClass('fix');
		}

		// активность пунктов в зависимости от скролла
		var $activeLink = null;
		if (scrollSize < SectionAdvantagesOffset) {
			var $activeLink = $('.top-menu__link[href="#init"]').parent();

		} else if ((scrollSize >= SectionAdvantagesOffset) && (scrollSize < SectionProcessOffset) ) {
			var $activeLink = $('.top-menu__link[href="#advantages-section"]').parent();

		} else if ((scrollSize >= SectionProcessOffset) && (scrollSize < ContactsVisibleHeight) && (scrollSize < SectionContactsOffset)) {
			var $activeLink = $('.top-menu__link[href="#process-section"]').parent();

		} else {
			var $activeLink = $('.top-menu__link[href="#contacts-section"]').parent();
		}

		$activeLink.switchClass('active');
	});


});