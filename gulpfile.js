var gulp         = require('gulp'),
	plumber      = require('gulp-plumber'),		 // уведомления об ошибках
	autoprefixer = require('gulp-autoprefixer'), // установка префиксов
	notify       = require('gulp-notify'),       // всплывающие уведомления
	imagemin     = require('gulp-imagemin'),	 // минификация изображений
	newer        = require('gulp-newer'),        // ограничение выборки для ускорения компиляции
	sass         = require('gulp-sass'),         // компилятор sass на C без compass
	rimraf       = require('rimraf'),            // удаление файлов
	browserSync  = require('browser-sync'),      // livereload
	connectPhp   = require('gulp-connect-php'),  // локальный сервер + PHP
	reload       = browserSync.reload,
	spritesmith  = require('gulp.spritesmith');

/**
 * Пути
 */
var path = {
	build: {
		js:     'build/js/',
		css:    'build/css/',
		img:    'build/img/',
		pic:    'build/pic/',
		fonts:  'build/fonts/',
		sprite: './build/img/sprite/'
	},
	src: {
		html:    'index.html',
		js:      'assets/js/**/*.js',
		sass:    'assets/sass/**/*.sass',
		img:     ['assets/img/**/*.*','!assets/img/sprite/*.*'],
		pic:     'assets/pic/**/*.*',
		fonts:   'assets/fonts/**/*.*',
		sprite:  'assets/img/sprite/*.*',
		sprLang: 'assets/sass/global/'
	},
	clean: {
		build:   './build',
		modules: './node_modules'
	}
};


/**
 *  SASS
 */
gulp.task('sass',function () {
	gulp.src(path.src.sass)
		.pipe(sass.sync().on('error', sass.logError))
		.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
		.pipe(autoprefixer({
			browsers: ['last 12 versions','> 1%'],
			cascade: false,
			remove: false
		}))
		.pipe(gulp.dest(path.build.css))
		.pipe(reload({stream:true}));
});


/**
 * SPRITE
 */
gulp.task('sprite', function() {
	console.log(path.src.sprite);
	var spriteData =
	gulp.src(path.src.sprite)
		.pipe(spritesmith({
			imgName: 'sprite.png',
			cssName: '_sprite.sass',
			imgPath: '/'+ path.build.sprite +'sprite.png',
		}));

	spriteData.img.pipe(gulp.dest(path.build.sprite)); // путь, куда сохраняем картинку
	spriteData.css.pipe(gulp.dest(path.src.sprLang)); // путь, куда сохраняем стили
});


/**
 * HTML
 */
gulp.task('html', function () {
	console.log(path.src.html);
	gulp.src(path.src.html)
		.pipe(reload({stream:true}));
});



/**
 * JS
 */
gulp.task('js', function () {
	gulp.src(path.src.js)
		.pipe(newer(path.build.js))
		.pipe(gulp.dest(path.build.js))
		.pipe(reload({stream:true}));
});


/**
 * IMAGES
 */
gulp.task('img', function () {
	gulp.src(path.src.img)
		.pipe(newer(path.build.img))
		.pipe(imagemin({progressive: true }))
		.pipe(gulp.dest(path.build.img))
		.pipe(reload({stream:true}));

	gulp.src(path.src.pic)
		.pipe(newer(path.build.pic))
		.pipe(imagemin({progressive: true }))
		.pipe(gulp.dest(path.build.pic))
		.pipe(reload({stream:true}));
});


/**
 * FONTS
 */
gulp.task('fonts', function () {
	gulp.src(path.src.pic)
		.pipe(newer(path.build.pic))
		.pipe(gulp.dest(path.build.pic))
		.pipe(reload({stream:true}));
});



/**
 * SERVER (only for local development)
 */
gulp.task('browserSync', function() {
    browserSync({
        proxy: 'zibazamin.dev',
        port: 8080,
        open: false,
        notify: true
    });
});


/**
 * WATCH
 */
gulp.task('watch',function () {
	gulp.watch(path.src.sass,['sass']);
	gulp.watch(path.src.js,['js']);
	gulp.watch(path.src.sprite,['sprite','sass']);
	gulp.watch(path.src.img,['img']);
	gulp.watch(path.src.pic,['img']);
	gulp.watch(path.src.html,['html']);
});


/**
 * START
 */
gulp.task('default', ['sprite', 'sass', 'js', 'img', 'watch']);
gulp.task('local', ['sprite', 'sass', 'js', 'img', 'fonts', 'watch', 'browserSync']);

// Очистка билда
gulp.task('clean', function (cb) {
	rimraf(path.clean.build, cb);
});

// Очистка полная
gulp.task('full-clean', function (cb) {
	rimraf(path.clean.build, cb);
	rimraf(path.clean.modules, cb);
});